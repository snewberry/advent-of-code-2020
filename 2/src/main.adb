with Ada.Text_IO;       use Ada.Text_IO;
with GNAT.String_Split; use GNAT;

procedure main is

   Input : File_Type; -- File to open

   Num_Valid  : Integer := 0; -- Number of valid passwords
   Num_Valid2 : Integer := 0; -- Number of valid passwords

   Subs : String_Split.Slice_Set;

begin

   -- Open file
   Open (File => Input, Mode => In_File, Name => "input.txt");

   -- User output
   Put_Line ("Offending Passwords: ");

   -- Rewrite all values of array based on input
   while not End_Of_File (Input) loop
      declare
         Line       : String := Get_Line (Input);
         Min_Num    : Integer;
         Max_Num    : Integer;
         Check_Char : String := "0";
      begin

         String_Split.Create
           (S    => Subs, From => Line, Separators => (' ', '-', ':'),
            Mode => String_Split.Multiple);

         Min_Num :=
           Integer'Value
             (String_Split.Slice
                (S => Subs, Index => 1)); -- This is first number!
         --Put_Line ("Min is: " & Min_Num'Image);

         Max_Num :=
           Integer'Value
             (String_Split.Slice
                (S => Subs, Index => 2)); -- This is second number!
         --Put_Line ("Max is: " & Max_Num'Image);

         Check_Char :=
           (String_Split.Slice (S => Subs, Index => 3)); -- Check char
         -- Put_Line(Check_Char);

         declare
            Pw : String :=
              String_Split.Slice (S => Subs, Index => 4); -- Password
            Num_Occ : Integer := 0;

         begin

            for i in 0 .. Pw'Length - 1 loop -- Part 1 checks

               if (Pw (Pw'First + i) = Check_Char (Check_Char'First)) then
                  Num_Occ := Num_Occ + 1;
               end if;

            end loop;

            if (Num_Occ < Min_Num) then -- Violation

               --Put_Line (Line); -- Show violating pw
               null;

            elsif (Num_Occ > Max_Num) then -- Violation

               --Put_Line (Line); -- Show violating pw
               null;

            else -- No violation

               Num_Valid := Num_Valid + 1;

            end if;

            -- Start Part 2 Checks
            declare
               Pass1 : Boolean := False;
               Pass2 : Boolean := False;
            begin

               if (Pw (Pw'First + Min_Num - 1) = Check_Char (Check_Char'First))
               then

                  -- Pass 1, flag this guy
                  Pass1 := True;

               end if;

               if (Pw (Pw'First + Max_Num - 1) = Check_Char (Check_Char'First))
               then

                  -- Pass 2, flag this guy
                  Pass2 := True;

               end if;

               if (Pass1 xor Pass2) = True then

                  -- Passing case
                  Num_Valid2 := Num_Valid2 + 1;

               else

                  --Put_Line (Line); -- Show violating pw
                  null;

               end if;

            end;

         end;

      end;

   end loop;

   -- Final output
   Put_Line ("-[PART1]-----------");
   Put_Line ("-Total valid: " & Num_Valid'Image);
   Put_Line ("-[PART2]-----------");
   Put_Line ("-Total valid: " & Num_Valid2'Image);

   -- Exception
exception
   when End_Error =>
      if Is_Open (Input) then
         Close (Input);
      end if;

end main;

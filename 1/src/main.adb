with Ada.Text_Io; use Ada.Text_IO;


procedure main is

   Input : File_Type; -- File to open

   idx : Integer := 1; -- Index when needed for file

   Arr : array (1..1000) of Integer; -- Array

begin

   -- Init array to 2020
   -- If we sum with these values it'll never be right
   for i in 1..1000 loop
      Arr(i) := 2020;
   end loop;

   -- Open file
   Open (File => Input,
         Mode => In_File,
         Name => "input.txt");

   -- Rewrite all values of array based on input
   while not End_of_File(Input) loop
      declare
         Line : String := Get_Line (Input);
      begin
         Arr(idx) := Integer'Value(Line);
         idx := idx + 1;
      end;
   end loop;

   -- Check all values for sum (Part 1)
   Named_Loop:
   for i in 1..1000 loop -- Outer loop

      declare
         Int1 : Integer;

      begin
         Int1 := Arr(i);
         for j in 1..1000 loop -- Inner loop
            declare
               Int2 : Integer;
               Sum : Integer;
               Prod : Integer;
            begin
               Int2 := Arr(j);
               Sum := Int1 + Int2;
               if Sum = 2020 then
                  Prod := Int1 * Int2;
                  Put_Line("[PART1] Answer is: " & Prod'Image);
                  Put_Line("Operands were: " & Int1'Image
                           & " and " & Int2'Image);
                  exit Named_Loop;
               end if;
            end;
         end loop;
      end;
   end loop Named_Loop;

   -- Check all values for sum (Part 2)
   Named_Loop2:
   for i in 1..1000 loop -- Outer loop

      declare
         Int1 : Integer;

      begin
         Int1 := Arr(i);

         for j in 1..1000 loop -- Mid loop

            declare
               Int2 : Integer;

            begin
               Int2 := Arr(j);

               for k in 1..1000 loop -- Inner loop
                  declare
                     Int3 : Integer;
                     Sum : Integer;
                     Prod : Integer;
                  begin
                     Int3 := Arr(k);
                     Sum := Int1 + Int2 + Int3;
                     if Sum = 2020 then
                        Prod := Int1 * Int2 * Int3;
                        Put_Line("[PART2] Answer is: " & Prod'Image);
                        Put_Line("Operands were: " & Int1'Image
                                 & ", " & Int2'Image & " and "
                                 & Int3'Image);
                        exit Named_Loop2;
                     end if;

                  end;
               end loop;

            end;
         end loop;

      end;
   end loop Named_Loop2;

   -- Exception
exception
   when End_Error =>
      if Is_Open(Input) then
         Close(Input);
      end if;

end main;
